import React from 'react'

//Initializes a react context
const UserContext = React.createContext()

//Initializes a context provider 
//Gives us ability to provide a specific context through a component
export const UserProvider = UserContext.Provider

/*export {UserProvider}*/

export default UserContext

//export default = exporing the file as a whole
//export = include the variable 