//index.js is the entry point > connects the app component to the root element of HTML

import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'

/*const root = ReactDOM.createRoot(document.getElementById('root'))*/
const root = ReactDOM.createRoot(document.querySelector('#root'));
root.render(
  <>
    <App/>
  </>
);


//after the installation of babel the <React.StrictMode> color issue was resolved
//Temporarily removed <React.StrictMode> to avoid further issues