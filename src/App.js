//importable components

import './App.css';
import {useState} from 'react'
import {UserProvider} from './UserContext'
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import Courses from './pages/Courses'
import CourseView from './components/CourseView'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import ErrorPage from './pages/ErrorPage'
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <>
      {/*Provides the user context throughout any component inside of it.*/}
      <UserProvider value={{user, setUser, unsetUser}}>
        {/*Initializes that dynamic routing will be involved*/}
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/courses" element={<Courses/>}/>
              <Route path="/courses/:courseId" element={<CourseView/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>} />
              <Route path="*" element={<ErrorPage/>}/>
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;



        /*
          ACTIVITY 

          1. Create a login component
          2. Build a form within the login page that contains the following iput fields: 

            -Email
            - Password
            - Submit Button 
          3. Import the login page here in App.js
          4. Follow the flow of the register page where it implements 
          the binding state and input fields , as well as the useEffect for validating the form data 
          5. After the login form has been submitted, show an alert that states 'You are now logged in!'
          6. The input fields must be cleared after the alert shows

          ROUTE: Initializes the dynamic routing will be involved

        */


 

