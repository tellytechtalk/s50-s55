import {Form, Button} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login() {
	//Initializes the use of the properties from the UserProvider in App.js file
	const {user, setUser} = useContext(UserContext)


	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	//initialize useNavigate
	const navigate = useNavigate()

	//For determining if the button is disabled or not 
	const [isActive, setIsActive] = useState(false)

	const retrieveUser = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(result => {
			console.log(result)

			setUser({
				id: result._id,
				isAdmin: result.isAdmin

			})

		})
	}


	function authenticate(event){

		//clears out the input fields after form submission
		event.preventDefault()


		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(result => {
			if(typeof result.accessToken !== "undefined"){
				localStorage.setItem('token', result.accessToken)

				retrieveUser(result.accessToken)


				Swal.fire({
					title:"Login Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			}else{
				Swal.fire({
					title:"Authentication Failed",
					icon: "error",
					text: "Pasensya ka na, sa kathang isip kong ito"

				})
			}
		})
	}
		/*
		OPTION: (local storage)

		localStorage.setItem('email', email)

		setUser ({
			email: localStorage.getItem('email'),

		})

		setEmail('')
		setPassword('')
		navigate('/')

		alert('You have successfully Login!')*/
	

	useEffect(()=>{
		if(email !== '' && password !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	},[email, password]);

	
	/*console.log(email)
	console.log(password1)
	console.log(password2)*/

	return (
		(user.id !== null)?
			<Navigate to = "/courses"/>
			:
		<>
		<h5 className = "login">LOGIN HERE:</h5>
		<Form onSubmit = {event => authenticate(event)}>
		   <Form.Group controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			            type="email" 
			            placeholder="Enter email" 
			            value = {email}
			            onChange = {event => setEmail(event.target.value)}
			            required
			        />
		    </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value = {password}
			        onChange = {event => setPassword(event.target.value)}
	                required
                />
            </Form.Group>

 
            { isActive ?
            	<Button variant="primary" type="submit" id="submitBtn">
            		Submit
           	 	</Button>
           	 	:
           	 	<Button variant="primary" type="submit" id="submitBtn" disabled>
            		Submit
           	 	</Button>
            }
            
		</Form>
		</>
		
	)
}