import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


export default function Home(){
	return(
		<>
			<Banner/>
	        <Highlights/>
	       
		</>

		/*

		ACTIVITY
		1. Create a 'Courses' component with a bootstrap card inside of it 
		2. The courses component must have a title , description 
		price , and button to enroll. All of these should be inside a card from bootstrap
		3. Import the courses component to the Home.js file and successfully display it upon recompilation 
		4. Push the discussion folder to GItlab under the S50-S55 repository with a commit message of 'Finished Activity for S50'
		5. Link the repository to Boodle 



		*/
 		
	)
}