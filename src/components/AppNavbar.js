import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import {Link, NavLink} from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'


export default function AppNavbar(){

	//local form:
	/*const[user, setUser] = useState(localStorage.getItem('email'))*/

	//global form
	const {user} = useContext(UserContext)

	return(
		<Navbar bg="primary" expand="lg">
			<Navbar.Brand as ={Link} to ="/">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as ={NavLink} to = "/">Home</Nav.Link>
					<Nav.Link as ={NavLink} to = "/courses">Courses</Nav.Link>
					{ (user.id) ?
						<Nav.Link as = {NavLink} to = "/logout"> Logout </Nav.Link>
						:
						<>
						<Nav.Link as ={NavLink} to = "/login">Login</Nav.Link>
						<Nav.Link as ={NavLink} to = "/register">Register</Nav.Link>
						</>
					}
					
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}

//"as" = represents the behavior 
// Navlink = navigation link 
//localstorage: local state only- the scope is within the component only, it has to be converted to global state in order to become accessible 