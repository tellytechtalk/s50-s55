/*OPTIONAL IMPORTING SYNTAX: 
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';*/

import {useState, useEffect} from 'react'
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'



export default function CourseCard({course}){

//The props may be destructured by using a variable
  const {name, description, price, _id} = course;




  //Using the state; state can be pretty much anything, the first index is the variable, the 2nd is what you want to do with the variable
  //initialize a 'count' state with a value of zero (0)
  const [count, setCount] = useState(0);
  const [slots,setSlots] = useState(15);
   const [isOpen, setIsOpen] = useState(true)

     /*   function enroll(){
              if(slots > 0) {
               setCount( count + 1)
               setSlots( slots - 1)
               return
            }
              alert('Slots are full!')
            }*/
//Effects in React JS is just like side effects/effecys in real life, where everything something happens within the component, a function of condition runs

//You may also listen or watch a specific state for changes instead of watching/listening to the whole component

         /* useEffect(() => {
            if(slots === 0){
              setIsOpen(false)
            }
        },[slots])*/
    
  
  return (
    <Card className="cardCourses">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle> 
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text> PHP {price}</Card.Text>
          <Link className = "btn btn-primary" to={`/courses/${_id}`}>Details</Link>
         {/* <Card.Text> Slots available: {slots}</Card.Text>
           <Card.Text> Is Open:{isOpen ? ' Yes ' : ' No '}</Card.Text>
          <Card.Text>Enrollees: {count}</Card.Text>
        <Button variant="primary" onClick={enroll}>Enroll</Button>*/}
      </Card.Body>
    </Card>
     
  );
}

//PropTypes can be used to validate the data comng from the props. You can define each property of the prop and assign specific validation for each of them. It is optional for added security.

CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description:PropTypes.string.isRequired,
    price:PropTypes.number.isRequired
  })
}

//in order to insert function and variables within jsx we use {open & close curly braces}
// props: built-in kay REACT, dito maaccess yung ba't-ibang properties 

/**
 ACTIVITY 

 1. Initialize 'slots' state that has a default value of 15 
 2. Include a logic inside the enroll function that counts down to zero when the enroll button is clicked 
 3. Show an alert once slots get to zero
 4. Show the number of slots along with the number of enrollees on the card
 

       ALTERNATE SOLUTION: 
        const [count, setCount] = useState(0)
        const [slots, setSlots] = useState(15)

        
 function enroll(){
          if (count < 15){
            setSlotCount(slotCount -1 )
            setCount(count + 1)
         }
          else 
         {
            alert("All slots have been filled!")
          }


**/